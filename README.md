# INDIANA
Scraping data about inmates in the state of Indiana for the art project Draconian Sentencing.

## Att göra

- [x] Undersöka sajt
- [x] Bestämma strategi (bash-scriptet find-inmate-numbers.sh som letar reda på url:er som motsvaras av en fånge och listar dem i url-list.csv)
- [x] Mejla eller skapa ParseHub-projekt
- [ ] Hämta data
- [ ] Rensa med Jupyter Notebook
- [ ] Exportera csv-fil
- [ ] Exportera Excel-fil
