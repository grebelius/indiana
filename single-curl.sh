#!/bin/bash

urlToTest="http://www.in.gov/apps/indcorrection/ofs/ofs?offnum=$1&search2.x=0&search2.y=0"
downloadedPage=$(curl "$urlToTest" -s 2>&1 | head -c 800)

if [[ $downloadedPage == *"><br>No information"* ]]
then
  succesState="404"
else
  succesState="200"
fi

echo "$1,$urlToTest,$succesState" >> url-list-with-success-state.csv
echo -ne "  $1 \r"