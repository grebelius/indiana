#!/bin/bash

# Find largest allready tried number
largestNumber=$(sort -nrk1,1 url-list-with-success-state.csv | head -1 | cut -d ' ' -f3 | awk -F "\"*,\"*" '{print $1}')
largerNumber=$((largestNumber+1))

seq $largerNumber 999999 | xargs -n 1 -P 100 ./single-curl.sh